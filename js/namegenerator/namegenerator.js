class Namegenerator{
	setFirstnames(firstnames){
		this.firstnames = firstnames;
	}

	setLastnames(lastnames){
		this.lastnames = lastnames
	}

	getRandomName(){
		var firstnameIndex = this.getRandomNumber(this.firstnames.length);
		var lastnameIndex = this.getRandomNumber(this.lastnames.length);

		var firstname = this.firstnames[firstnameIndex];
		var lastname = this.lastnames[lastnameIndex];

		return firstname+" "+lastname;
	}

	getRandomNumber(max){
		return Math.round(Math.random() * max)
	}
}
