// Class
function Keypresser(){
	var preventRepeat = true;
  var codeEventsPressed = {};
  var codeEventsReleased = {};
  var keys = {};
  var globalPressed;
  var globalReleased;
  
  this.down = function(e){
    if(preventRepeat && keys[e.keyCode] && keys[e.keyCode] == 1){
      return;
    }
    if(codeEventsPressed[e.keyCode]){
    	codeEventsPressed[e.keyCode](e);
    }
    
    if(globalPressed){
    	globalPressed(e);
    }
    
    keys[e.keyCode] = 1;
  }
  this.up = function(e){
    if(codeEventsReleased[e.keyCode]){
    	codeEventsReleased[e.keyCode](e);
    }
    if(globalReleased){
    	globalReleased(e);
    }
    keys[e.keyCode] = 0;
  }

	this.registerKeyCodePressed = function(keyCode, func){
		codeEventsPressed[keyCode] = func;
  }
  
  this.registerKeyCodeReleased = function(keyCode, func){
  	codeEventsReleased[keyCode] = func;
  }
  
  this.registerGlobalPressed = function(func){
  	globalPressed = func;
  }
  
  this.registerGlobalReleased = function(func){
  	globalReleased = func;
  }
  
  window.addEventListener("keyup", this.up, false);
  window.addEventListener("keydown", this.down, false);
}



