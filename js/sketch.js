var ship;

var socket;
var kp = new Keypresser();
var shots = [];
var explosions = [];
var enemies = {};
var config = new Config();
var ng = new Namegenerator();
function setup(){

	// Creating canvas
	createCanvas(400,400);

	// Setting p5js to use degrees and not radians
	angleMode(DEGREES);

	setupServer();
	setupControls();
	setupSocket();
	setupNames();
	setupShip();
}

function setupNames(){
	// Setting up namegenerator. Vars firstnames and lastnames are loaded in js/firstnames.js and js/lastnames.js
	ng.setFirstnames(firstnames);
	ng.setLastnames(lastnames);
}

function setupShip(){
	// Creating a new ship. This has to be done as a part of setup() since p5js is not initialized before
	ship = new Ship();
	ship.fill = color(0,0,255);
	ship.name = ng.getRandomName();
}

function setupServer(){
	// Choosing which server and port to connect to
	config.set('host','http://localhost');
	config.set('port',3000);
}

function setupControls(){
	// Setting up config. 
	// Not setting directly in registerKeyCodePressed to keep this grouped
	config.set('up', 38);
	config.set('left',37);
	config.set('down',36);
	config.set('right', 39);
	config.set('shoot', 32);

	// Registering eventhandlers with the Keypresser
	kp.registerKeyCodePressed(config.get('left'),function(){
		ship.turningLeft = true;
	});

	kp.registerKeyCodeReleased(config.get('left'), function(){
		ship.turningLeft = false;
	});

	kp.registerKeyCodePressed(config.get('right'), function(){
		ship.turningRight = true;
	});

	kp.registerKeyCodeReleased(config.get('right'), function(){
		ship.turningRight = false;
	});

	kp.registerKeyCodePressed(config.get('up'), function(){
		ship.boosting = true;
	});

	kp.registerKeyCodeReleased(config.get('up'), function(){
		ship.boosting = false;
	});

	kp.registerKeyCodePressed(config.get('shoot'), function(){
		ship.shoot();
		socket.emit('shot', {
			pos:{
				x:ship.pos.x,
				y:ship.pos.y
			},
			direction:ship.rotation});
	});
}

function setupSocket(){
	// Creating a socket connection to whatever server and port is set in the config
	socket = io.connect(config.get('host')+':'+config.get('port'));

	// Registering events coming from the server
	socket.on('enemies', setEnemies);
	socket.on('shot', setShot);
	socket.on('disconnected', removeEnemy);
	socket.on('connect',setShipId);
}

function setShipId(){
	// Setting ship id to the socket id. 
	ship.id = socket.id;
}

function draw(){
	// Drawing everything 
	background(0);
	// Sending "ship" event to server.
	// This event tells about the ships position, score and name
	socket.emit('ship', {
		pos:{
			x:ship.pos.x, 
			y:ship.pos.y
		}, 
		rotation:ship.rotation,
		score:ship.score,
		name:ship.name
	});
	// Update and draw the ship
	ship.update();
	ship.draw();

	// Update other things 
	updateAndDrawShots();
	bulletDetection();
	drawEnemies();
	drawScores();
	updateExplosions();
}

function drawScores(){
	// Drawing the score on the screen
	push();
	var y = height - 15;
	textStyle(BOLD);
	for(a in enemies){
		if(a == ship.id){
			fill(0,0,255); 	// The players score is blue
		}else{
			fill(255,0,0);	// Enemies' score is red
		}
		
		var str = enemies[a].name+": "+enemies[a].score;
		text(str, 10, y);
		y-=15;
	}
	pop();
}

function drawEnemies(){
	// Drawing enemies
	for(a in enemies){
		if(a == socket.id){
			continue;
		}
		enemies[a].draw();	
	}
}

function updateAndDrawShots(){
	// Updating shots and draw them
	for(var idx = shots.length-1; idx >= 0;idx--){
		shots[idx].update();
		shots[idx].draw();
		if(shots[idx].isOutOfBounds()){
			shots.splice(idx,1);
		}
	}
}


function bulletDetection(){
	// Detects if an enemy is colliding with a bullet.
	for(a in enemies){
		// Looping backwars, to avoid problems with iterating over an deleted object
		for(var idx = shots.length-1; idx >= 0;idx--){
			// If the player hits itself, just ignore
			if(enemies[a].id == shots[idx].owner){
				continue;
			}

			if(enemies[a].hitBy(shots[idx])){
				if(shots[idx].owner == ship.id){
					ship.score++;
				}
				explosions.push(new Explosion(enemies[a].pos.x, enemies[a].pos.y));
				shots.splice(idx, 1);
			}
		}
	}
	return false;
}

function updateExplosions(){
	// Drawing explosions
	for(var idx = explosions.length-1; idx>=0; idx--){
		explosions[idx].update();
		explosions[idx].draw();
		if(explosions[idx].isDone()){
			explosions.splice(idx,1);
		}
	}
}

function removeEnemy(id){
	// Remove enemy on e.g. disconnect
	console.log("User "+id+" has disconnected");
	delete enemies[id];
}

function setEnemies(remote_enemies){
	// Showing enemies at their given positions
	for(a in remote_enemies){
		var current = remote_enemies[a];
		if(!enemies[a]){
			// If the enemy does not exists in the local object, then create the enemy
			console.log("Got new enemy with id = ", a);
			var enemy = new Ship();
			enemy.fill = color(255,0,0);
			enemy.id = a;
			enemies[a] = enemy;
		}

		enemies[a].pos.x = current.pos.x;
		enemies[a].pos.y = current.pos.y;
		enemies[a].rotation = current.rotation;
		enemies[a].score = current.score;
		enemies[a].name = current.name;
	}
}

function setShot(data){
	// Adding an enemy shot to the array of shots
	var options = {
		pos:{
			x: data.pos.x,
			y: data.pos.y
		},
		owner:data.owner,
		direction:data.direction
	}
	var s = new Shot(options);

	shots.push(s);
}

