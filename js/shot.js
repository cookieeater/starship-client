class Shot{
	constructor(options){
  	this.radius = 2;
  	this.pos = createVector(options.pos.x, options.pos.y);
    this.vel = p5.Vector.fromAngle(radians(options.direction)).mult(10);
    this.owner = options.owner;
  }
  
  update(){
	  this.pos.add(this.vel);
  }
  
  draw(){
  	push();
    	fill(255);
			ellipse(this.pos.x, this.pos.y, this.radius*2);
    pop();
  }
  
  isOutOfBounds(){
  	var out = this.pos.x > width || this.pos.x < 0 || this.pos.y > height || this.pos.y < 0;
		return out;
  }
}