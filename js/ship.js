
class Ship{
  constructor(){
  	this.rotation = 0;
  	this.pos = createVector(width/2, height/2);
    this.vel = createVector(0,0);
    this.acc = createVector(0,0);
    this.width = 6
    this.dent = 2;
    this.turning = 0;
    this.boosting = false;
    this.turningRight = false;
    this.turningLeft = false;
    this.shots = [];
    this.fill;
    this.score = 0;
  }

  draw(){ 
  	push(); 
      stroke(255);
      translate(this.pos.x, this.pos.y);
      rotate(this.rotation);
      fill(this.fill);
      beginShape();

        vertex(-this.dent,0);
        vertex(-this.width,-this.width);
        vertex(this.width, 0);
        vertex(-this.width, this.width);
       endShape(CLOSE);
     pop();
  }
  
  update(){
    this.turn()
    this.pos.add(this.vel);
    this.vel.mult(0.99);
    if(this.boosting){
      this.boost();
    }

    if(this.pos.x > width){
      this.pos.x = 0;
    }
    if(this.pos.x < 0){
      this.pos.x = width;
    }

    if(this.pos.y > height){
      this.pos.y = 0;
    }

    if(this.pos.y < 0){
      this.pos.y = height;
    }

  }

  turn(){
    if(this.turningLeft && this.turningRight){
      this.turning = 0;
    }else if(this.turningLeft){
      this.turning = -4;
    }else if(this.turningRight){
      this.turning = 4;
    }else{
      this.turning = 0;
    }
    this.rotation += this.turning;
  }

  boost(){
    var force = p5.Vector.fromAngle(radians(this.rotation));
    force.mult(0.1);
    this.applyForce(force);
  }

  applyForce(force){
    this.vel.add(force);
  }

  hitBy(obj){
    var d = dist(obj.pos.x, obj.pos.y, this.pos.x, this.pos.y);
    
    if(d < this.width*2){
      console.log(obj.owner +" shot "+this.id);
      return true;
    }

    return false;;
  }

  shoot(){
    var options = {
      pos: {
        x: this.pos.x,
        y: this.pos.y
      },
      direction: this.rotation,
      owner: this.id
    }
    var shot = new Shot(options);
    
    shots.push(shot);
  }
}
