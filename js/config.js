class Config{
	constructor(items){
	  	if(items){
			this.storage = items;
	    }else{
			this.storage = {};    
	    }
  	}  
  
	  get(key){
		return this.storage[key];
	  }
	  
	  set(key, value){
		this.storage[key] = value;
	  } 
}