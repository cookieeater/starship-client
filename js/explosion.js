function Explosion(x, y){
	this.width = 0;
	this.maxWidth = 20;
	this.expansion = 3;
	this.fill = 255;
	this.pos = {
		x:x,
		y:y
	}

	this.draw = function(){
		push();
			fill(this.fill);
			ellipse(this.pos.x, this.pos.y, this.width, this.width);
		pop();
	}

	this.update = function(){
		this.width+=this.expansion;
	}

	this.isDone = function(){
		return this.width>=this.maxWidth;
	}
}